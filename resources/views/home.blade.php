@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row font-weight-bold shadow-lg bg-purple mb-2 " align="center" >
        <div class="col-md-12 m-5">
            <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-primary font-weight-bold ">Receipt</button>
                <button type="button" class="btn btn-primary font-weight-bold">Won</button>
                <button type="button" class="btn btn-primary font-weight-bold">Payments</button>
            </div>
        </div>
    </div>
    <div class="row mt-lg-5">
        <div class="col-md-5 ml-lg-5 bg-dark ml-2" style="overflow-y: auto;height: 300px;box-shadow: 4px 2px 4px #000000">
            <h4 class="font-weight-bold p-3 text-white text-uppercase" style="background-color: deeppink">show</h4>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">01</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">02</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">03</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">04</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">05</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">06</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">07</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">08</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">09</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">10</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">11</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">12</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">13</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">14</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">15</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">16</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">17</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">18</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">19</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">20</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">21</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">22</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">23</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">24</button>
            </div>
            <div class="btn-group" aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">25</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">26</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">27</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">28</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">29</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">30</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">31</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">32</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">33</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">34</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">35</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">36</button>
            </div>
            <div class="btn-group" aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">37</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">38</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">39</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">40</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">41</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">42</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">43</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">44</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">45</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">46</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">47</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">48</button>
            </div>
            <div class="btn-group" aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">49</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">50</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">51</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">52</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">53</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">54</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">56</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">57</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">58</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">59</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">60</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">61</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">62</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">62</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">63</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">64</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">65</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">66</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">67</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">68</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">69</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">70</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">71</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">72</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">73</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">74</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">75</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">76</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">77</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">78</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">79</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">80</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">81</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">82</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">83</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">84</button>
            </div>
            <div class="btn-group " aria-label="First group">
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">85</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">86</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">87</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">88</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">89</button>
                <button class="bg-success btn-primary text-white p-3 font-weight-bold" style="font-size: 1.5em;border-radius: 40px;margin: 5px">90</button>
            </div>
        </div>
        <div class="col-md-5 text-white ml-lg-5 bg-dark ml-2" style="overflow-y: auto;height: 250px;box-shadow: 4px 2px 4px #000000">
            <h4 class="font-weight-bold p-3 text-white text-uppercase" style="background-color: deeppink">show</h4>
             <form class="mb-3">
            <div class="form-group">
            <label for="inputAddress" class="text-uppercase">your numbers</label>
            <input type="text" name="perm_numbers" class="form-control" id="inputAddress" readonly="" required>
            </div>
            <div class="form-row">
            <div class="form-group col-md-6">
            <label for="inputEmail4" class="text-uppercase">amount</label>
            <input type="text" name="perm_amounts" class="form-control" required>
            </div>
            <div class="form-group col-md-6">
            <label for="inputPassword4" class="text-uppercase">total</label>
            <input type="text" name="total_amount" class="form-control" readonly="" id="inputPassword4" required>
            </div>
            </div>

            <button type="submit" class="btn btn-primary">preview</button>
            </form>
        </div>
</div>
</div>
@endsection
