<footer class="text-md-left text-center ">
    <div class="container text-white pt-5">
        <div class="row">
            <div class="col-md-4">
                <h5 class="section-title">QUICK LINKS</h5>
                <ul>
                    <li><a href="">HOME</a></li>
                    <li><a href="">ABOUT US</a></li>
                    <li><a href="">PRODUCTS</a></li>
                    {{--<li><a href="{{route('services')}}">WHAT WE DO</a></li>--}}
                    {{--<li><a href="{{route('team')}}">TEAM</a></li>--}}
                    <li><a href="">CONTACT US</a></li>
                </ul>
            </div>
            <div class="col-md-4 text-center">
                {{--<img src="/images/logo-1.png" class="img-fluid footer-logo" alt="">--}}
                <h5 class="text-uppercase section-title">Footer Content</h5>
                <p>YOUR LIVING IS WHAT WE CARE AND TO MAKE SURE THAT ALL YOUR FINANCIAL
                    PROBLEMS IS SOLVED THROUGH OUR POTENTIAL SERVICE.
                    PLEASE DO NOT FORGET TO GIVE US YOUR FEEDBACK OR LEAVE AS ANY MESSAGE OF YOUR CHOICE. THANK YOU</p>
            </div>
            <div class="col-md-4">
                <h5 class="section-title">CONTACT INFO</h5>
                <ul>
                    <li class="text-white footer-contact"><img src="/svg/call.svg" alt="" width="12" height="12"> 78853678728389</li>
                    <li class="text-white footer-contact"><img src="/svg/email.svg" alt="" width="12" height="12"> winlottorygh.com</li>
                    <li class="text-white footer-contact"><img src="/svg/marker.svg" alt="" width="12" height="12">Located At Assin Fosu</li>
                </ul>
            </div>
            <div class="col-12 text-center copyright-text mb-xl-5">COPYRIGHT &COPY {{DATE("Y")}} WIN LOTTORY GHANA LIMITED</div>
            <div class="container">

                <!-- Social buttons -->
                <ul class="list-unstyled list-inline text-center">
                    <li class="list-inline-item">
                        <a class="btn-floating btn-fb mx-1">
                            <i class="fab fa-facebook-f"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-tw mx-1">
                            <i class="fab fa-twitter"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-gplus mx-1">
                            <i class="fab fa-google-plus-g"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-li mx-1">
                            <i class="fab fa-linkedin-in"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-dribbble mx-1">
                            <i class="fab fa-dribbble"> </i>
                        </a>
                    </li>
                </ul>
                <!-- Social buttons -->

            </div>
        </div>

    </div>
</footer>
