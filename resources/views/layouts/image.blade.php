<section class="section">
    <div class="container">
        <h3 class="text-center section-title">OUR CLIENTS</h3>
        <div class="owl-carousel owl-theme partners mt-5" data-aos="fade-up" data-aos-duration="1000">
            <div class="item" data-merge="1"><img src="/images/partners/promasidor.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/gandour.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/kasapreko.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/abl.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/belaqua.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/voltic.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/funmilk.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/unilever.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/gihoc.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/guiness.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/nutrifood.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/healthlife.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/olam.png" alt=""></div>
            <div class="item" data-merge="1"><img src="/images/partners/nestly.png" alt=""></div>

        </div>

    </div>
</section>
@section('extra-scripts')
    <script src = "/js/owl.carousel.min.js"></script>
    <script>
        // $('.owl-carousel').owlCarousel({
        //     items:6,
        //     loop:true,
        //     margin:10,
        //     merge:true,
        //     responsive:{
        //         678:{
        //             mergeFit:true
        //         },
        //         1000:{
        //             mergeFit:false
        //         }
        //     }
        // });

        // $('.owl-carousel').owlCarousel({
        //     loop:true,
        //     margin:10,
        //     responsiveClass:true,
        //     responsive:{
        //         0:{
        //             items:2,
        //             nav:true
        //         },
        //         600:{
        //             items:3,
        //             nav:true
        //         },
        //         1000:{
        //             items:6,
        //             nav:true,
        //             loop:true
        //         }
        //     }
        // })

        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        })
    </script>