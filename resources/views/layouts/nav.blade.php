<nav class="navbar navbar-expand-lg navbar-light  shadow-lg pb-xl-5 pt-3" style="background-color: rebeccapurple">
    <a class="navbar-brand" href="#"><img src="/images/images.jpg" width="30" height="30"></a>
    <button class="navbar-toggler cta-bg-primary text-white" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon navbar-light"></span>
    </button>
    <div class="contact-info ml-6">
        <ul class="text-wrapper">
            <li class="text-white section-title"><img src="/svg/email.svg" alt="" width="12" height="12"><span class="letters"> am@gmail.com</span>  &nbsp;&nbsp;</li>
            <li class="text-white section-title"><img src="/svg/call.svg" alt="" width="12" height="12"><span class="letters"> 76235778589768</span></li>
        </ul>
    </div>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        {{--@if (Route::has('login'))--}}
            {{--<div class="top-center links bg-dark p-3">--}}
                {{--@auth--}}
                    {{--<a href="{{ url('/home') }}">Home</a>--}}
                {{--@else--}}
                    {{--<a href="{{ route('login') }}">Login</a>--}}

                    {{--@if (Route::has('register'))--}}
                        {{--<a href="{{ route('register') }}">Register</a>--}}
                    {{--@endif--}}
                {{--@endauth--}}
            {{--</div>--}}
        {{--@endif--}}
        <div class="navbar-nav text-center">
            <li class="nav-item">
                <a class="nav-link text-white hvr-underline-from-center section-title" href="">HOME</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white hvr-underline-from-center section-title" href="">ABOUT US</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white hvr-underline-from-center section-title" href="">PRODUCTS</a>
            </li>
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link text-white hvr-underline-from-center" href="{{route('services')}}">WHAT WE DO</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link text-white hvr-underline-from-center" href="{{route('team')}}">TEAM</a>--}}
            {{--</li>--}}
            <li class="nav-item">
                <a class="nav-link text-white hvr-underline-from-center section-title" href="">CONTACT US</a>
            </li>
        </div>
    </div>

</nav>

