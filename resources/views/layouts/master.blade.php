<!DOCTYPE html>
<html lang="en">
<style>
    /*html, body {*/
        /*background-color: #fff;*/
        /*color: #636b6f;*/
        /*font-family: 'Nunito', sans-serif;*/
        /*font-weight: 200;*/
        /*!*height: 100vh;*!*/
        /*margin: 0;*/
    /*}*/

    /*.full-height {*/
    /*height: 100vh;*/
    /*}*/

    /*.flex-center {*/
    /*align-items: center;*/
    /*display: flex;*/
    /*justify-content: center;*/
    /*}*/

    /*.position-ref {*/
    /*position: relative;*/
    /*}*/

    /*.top-right {*/
    /*position: absolute;*/
    /*right: 10px;*/
    /*top: 18px;*/
    /*}*/

    /*.content {*/
    /*text-align: center;*/
    /*}*/

    /*.title {*/
    /*font-size: 84px;*/
    /*}*/

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    /*.m-b-md {*/
    /*margin-bottom: 30px;*/
    /*}*/
</style>
@include('layouts.headers')
<body class="" style="background-color: #f8f9fa">
{{--<div class="container-fluid">--}}
    @include('layouts.home_nav')
    @yield('content')
    @include('layouts.footer')
{{--</div>--}}
<script src="/js/jquery.js"></script>
<script src="/js/app.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>

</body>
</html>
